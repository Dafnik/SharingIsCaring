let ua = window.navigator.userAgent;
let msie = ua.indexOf("MSIE ");

if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  // If Internet Explorer, return version number
{
  alert("It looks like your are using Internet Explorer! Internet Explorer is not supported! You can still access" +
    " the Website but there is no guarantee everything is working!");
}
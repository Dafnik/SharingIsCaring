if (getCookie("theme") === "dark") {
  changeTheme();
}

function changeTheme() {
  let nav_head = document.getElementById("nav_head");

  let body = document.getElementById("body");

  let myContainer = document.getElementById("mycontainer");

  let nav_side = document.getElementById("nav_side");

  let changeThemeBtn = document.getElementById("changeThemeBtn");

  //Change from white to black
  if (nav_head.classList.contains("white")) {
    setCookie("theme", "dark", "Thu, 18 Dec 2020 12:00:00 UTC");
    setTimeout(
      function () {
        changeThemeBtn.innerText = "Change to light mode";
      },
      150
    );

    nav_head.classList.remove("white");
    nav_head.classList.add("blue-grey");
    nav_head.classList.add("darken-4");

    nav_side.classList.remove("white");
    nav_side.classList.add("blue-grey");
    nav_side.classList.add("darken-2");

    body.classList.remove("white");
    body.classList.add("blue-grey");
    body.classList.add("darken-3");

    myContainer.classList.remove("white");
    myContainer.classList.add("blue-grey");
    myContainer.classList.add("darken-1");

    let whitetext = document.getElementsByClassName("colorable_text");

    for (let i = 0; i < whitetext.length; i++) {
      whitetext[i].classList.remove("black-text");
      whitetext[i].classList.add("white-text");
    }

    let dropdown = document.getElementsByClassName("colorable_dropdown");
    for (let i = 0; i < dropdown.length; i++) {
      dropdown[i].classList.remove("white");
      dropdown[i].classList.add("blue-grey");
      dropdown[i].classList.add("darken-1");
    }

    let links = document.getElementsByClassName("colorable_link");
    for (let i = 0; i < links.length; i++) {
      links[i].classList.add("blue-text");
      links[i].classList.add("text-lighten-2");
    }

  } else {
    setCookie("theme", "light", "Thu, 18 Dec 2020 12:00:00 UTC");
    setTimeout(
      function () {
        changeThemeBtn.innerText = "Change to dark mode";
      },
      150
    );

    nav_head.classList.remove("darken-4");
    nav_head.classList.remove("blue-grey");
    nav_head.classList.add("white");

    nav_side.classList.remove("blue-grey");
    nav_side.classList.remove("darken-2");
    nav_side.classList.add("white");

    body.classList.remove("blue-grey");
    body.classList.remove("darken-3");
    body.classList.add("white");

    myContainer.classList.remove("blue-grey");
    myContainer.classList.remove("darken-1");
    myContainer.classList.add("white");

    let blacktext = document.getElementsByClassName("colorable_text");

    for (let i = 0; i < blacktext.length; i++) {
      blacktext[i].classList.remove("white-text");
      blacktext[i].classList.add("black-text");
    }

    let dropdown = document.getElementsByClassName("colorable_dropdown");
    for (let i = 0; i < dropdown.length; i++) {
      dropdown[i].classList.remove("blue-grey");
      dropdown[i].classList.remove("darken-1");
      dropdown[i].classList.add("white");
    }

    let links = document.getElementsByClassName("colorable_link");
    for (let i = 0; i < links.length; i++) {
      links[i].classList.remove("blue-text");
      links[i].classList.remove("text-lighten-2");
    }
  }
}
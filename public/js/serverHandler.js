let sidenav = document.querySelector('.sidenav');
M.Sidenav.init(sidenav, { });

let dropdown = document.querySelectorAll('.dropdown-trigger');
M.Dropdown.init(dropdown, { hover: true });

let modal = document.querySelectorAll('.modal');
M.Modal.init(modal, { });

let action_btn = document.querySelectorAll('.fixed-action-btn');
M.FloatingActionButton.init(action_btn, { direction: 'left' });

let tooltipped = document.querySelectorAll('.tooltipped');
M.Tooltip.init(tooltipped, { });

function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  var expires = "expires="+d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function showAdminMenuTapTarget() {
  let adminMenuTapTarget = document.querySelector('#adminMenuTapTarget');
  let instance = M.TapTarget.init(adminMenuTapTarget, {  });

  instance.open();

  setTimeout(
    function () {
      instance.close();
    },
    5000
  );

}

function xhr_post(url, cfunc, param) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      cfunc(xhttp);
    }
  };
  xhttp.open("POST", url, true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send(param);
}
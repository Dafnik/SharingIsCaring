<?php

ini_set('display_errors', 'On');
error_reporting(E_ALL | E_STRICT);

if (!file_exists(__DIR__ . '/../private/settings.json')) {
  header("Location: ./setup.php");
  die();
}

/*
 * Possible alerts:
 * loginSuccessful | wrongCredentials | logoutSuccessful | directory_exists | successful_created_directory | permission_denied
 * fileExists | fileToBig | file_successful_uploaded | file_uploaded_error | successful_deleted_dir | file_not_exists
 * successful_deleted_file | successful_cache_cleared
 */
$alert = "none";

require_once('./logic/fileHandler.php');
require_once('./logic/imageHandler.php');
require_once('./logic/settingsHandler.php');
require_once('./logic/authHandler.php');
require_once('./logic/alertHandler.php');
require_once('./logic/modalHandler.php');
require_once('./../private/filter.php');

$isInSubdirectory = false;
$subdirectorypath = "";
if (isset($_REQUEST['directory'])) {
  $isInSubdirectory = true;
  $subdirectorypath = $_REQUEST['directory'];
}

if (isset($_REQUEST['action'])) {
  switch ($_REQUEST['action']) {
    case 'login':
      if (isset($_REQUEST['username']) AND isset($_REQUEST['password'])) {
        $alert = login($_REQUEST['username'], $_REQUEST['password']);
      }
      break;
    case 'logout':
      logout();
      $alert = "logoutSuccessful";
      break;

    case 'create_directory':
      if (isset($_REQUEST['directory_name'])) {
        if (!isset($_REQUEST['directory'])) {
          $_REQUEST['directory'] = null;
        }
        $alert = createDirectory($_REQUEST['directory'], $_REQUEST['directory_name']);
      }
      break;

    case 'upload_a_file':
      if (!isset($_REQUEST['directory'])) {
        $_REQUEST['directory'] = null;
      }

      $total = count($_FILES['fileToUpload']['name']);

      for( $i=0 ; $i < $total ; $i++ ) {
        $alert = uploadAFile($_REQUEST['directory'], basename($_FILES["fileToUpload"]["name"][$i]),
          $_FILES["fileToUpload"]["size"][$i], $_FILES["fileToUpload"]["tmp_name"][$i]);
      }

      break;

    case 'delete':
      if (isset($_REQUEST['file'])) {
        $alert = delete($_REQUEST['file']);
      }

      break;

    case 'clearCache':
      $alert = clearCache();

      break;
  }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Sharing Is Caring</title>

  <link rel="shortcut icon" type="image/favicon.ico" href="images/favicon.ico">

  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>

  <!-- CSS  -->
  <link rel="stylesheet" href="css/venobox.css" type="text/css" media="screen"/>
  <link href="fonts/materialicon.css" rel="stylesheet">
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body id="body" class="colorable_body white">

<!-- Dropdown Structure -->
<ul id="dropdown_nav" class="dropdown-content colorable_dropdown">
  <li><a class="colorable_text black-text" href="https://dafnik.me" target="_blank">dafnik.me</a></li>
  <li><a class="waves-effect waves-green colorable_text black-text modal-trigger" href="#modal_about">Info</a></li>
</ul>

<div class="navbar-fixed">
  <nav>
    <div id="nav_head" class="nav-wrapper white" style="padding-left: 10px;">
      <a href="#" data-target="nav_side" class="sidenav-trigger hide-on-med-and-up colorable_text black-text"><i
            class="material-icons">menu</i></a>
      <ul class="left">
        <li>
          <a class="left waves-effect waves-purple colorable_text black-text breadcrumb" href="index.php">Root</a>
          <?php
          if ($isInSubdirectory) {
            $x = explode('/', $subdirectorypath);

            for ($i = 0; $i < count($x); $i++) {
              echo '<a class="left waves-effect waves-purple colorable_text black-text breadcrumb" href="./index.php?directory=';

              for ($j = 0; $j < $i + 1; $j++) {
                if ($j == 0) {
                  echo $x[$j];
                } else {
                  echo '/' . $x[$j];
                }
              }

              echo '">' . $x[$i] . '</a>';
            }
          }
          ?>

        </li>
      </ul>
    </div>
  </nav>
</div>

<ul id="nav_side" class="sidenav sidenav-fixed white">
  <li><a class="subheader">Authentication</a></li>
  <li>
    <?php

    if (getLoggedIn()) {
      echo '<a class="colorable_text black-text waves-effect waves-green modal-trigger" href="./index.php?action=logout"><i
          class="material-icons colorable_text black-text">lock</i>Logout</a>';
    } else {
      echo '<a class="colorable_text black-text waves-effect waves-green modal-trigger" href="#modal_login"><i
          class="material-icons colorable_text black-text">lock_open</i>Login</a>';
    }
    ?>

  </li>
  <li>
    <div class="divider"></div>
  </li>
  <?php

  if (getLoggedIn()) {
    echo '<li><a class="subheader">Administration</a></li>';

    echo '
      <li>
        <a class="waves-effect waves-teal colorable_text black-text tooltipped" data-position="right" 
            data-tooltip="Images are scaled down to provide a efficient preview">
          Cache size: ' . getAwesomeSize(getDirectorySize("./cache/")) . '
        </a>
      </li>
    ';

    echo '
      <li>
        <a class="waves-effect waves-red colorable_text black-text tooltipped" 
            href="./index.php?action=clearCache';
    if ($isInSubdirectory) echo '&directory=' . $subdirectorypath;
    echo '"
            data-position="right" data-tooltip="Please note that their is a README.md in the cache so it will not be complete empty!">
          Clear cache now
        </a>
      </li>
    ';

    echo ' <li><div class="divider"></div></li>';
  }

  ?>
  <li><a class="subheader">Theming</a></li>
  <li><a id="changeThemeBtn" class="waves-effect waves-orange colorable_text black-text" onclick="changeTheme()">Change
      to dark mode</a></li>
  <li>
    <div class="divider"></div>
  </li>

  <li><a class="subheader">Informations</a></li>
  <li>
    <a class="waves-effect waves-teal colorable_text black-text">Logged in:
      <?php if (getLoggedIn()) echo "true"; else echo "false"; ?>
    </a>
  </li>
  <li>
    <a class="waves-effect waves-teal colorable_text black-text">Latest alert:
      <?php echo $alert; ?>
    </a>
  </li>
  <!-- Dropdown Trigger -->
  <li>
    <a class="dropdown-trigger colorable_text black-text" href=""
       data-target="dropdown_nav">About<i class="material-icons right colorable_text black-text">arrow_drop_down</i>
    </a>
  </li>

  <li class="colorable_text black-text"
      style="bottom: 0; margin-bottom: 60px; position: fixed; text-align: center; width: 100%">
    <div class="fancy"> powered by</div>
    <a class="waves-effect waves-green colorable_text black-text" target="_blank"
       href="https://github.com/Dafnik/SharingIsCaring/">SharingIsCaring</a>
  </li>
</ul>

<div id="mycontainer" class="card-panel white">
  <div class="row">
    <div class="col s12 m12 lg12 xl12">
      <table class="colorable_text black-text responsive-table">
        <thead>
        <tr>
          <th>Name</th>
          <th>Preview</th>
          <th>Last modified</th>
          <th>Size</th>
          <th>Actions</th>
          <?php if (getLoggedIn()) echo '<th>Delete</th>' ?>
        </tr>
        </thead>

        <tbody>
        <?php
        $rootDirectory = './files/';
        $cacheDirectory = './cache/';

        if ($isInSubdirectory) {
          $directory = $rootDirectory . $subdirectorypath . '/';
        } else {
          $directory = $rootDirectory;
        }

        $folderIsEmpty = true;

        $directory_data = getDataFromDirectory($directory);

        foreach ($directory_data as $data) {
          $folderIsEmpty = false;

          $exact_path = $directory . $data;

          //Check if some file should not be shown
          if (!in_array($exact_path, $filter)) {
            $public_path = '';

            if ($isInSubdirectory) {
              $public_path = $subdirectorypath . '/' . $data;
            } else {
              $public_path = $data;
            }

            $isDirectory = false;
            $size = 0;
            $link = "";
            $preview = '¯\_(ツ)_/¯';
            $sharePath = '';
            $prePath = (isset($_SERVER['HTTPS']) ? "https" : "http") . '://' . $_SERVER['HTTP_HOST'];

            if (isDirectory($exact_path)) {
              $isDirectory = true;

              $sharePath = $prePath . '/index.php?directory=' . $public_path;

              $size = getDirectorySize($exact_path);
              $link = '<a class="colorable_link" href="index.php?directory=' . $public_path . '">' . $data . '</a>';
            } else {
              $sharePath = $prePath . '/files/' . $public_path;

              $size = getFileSize($exact_path);
              $link = '<a class="colorable_link" target="_blank" href="' . $exact_path . '">' . $data . '</a>';

              if ((strpos($data, 'jpg') !== false) || (strpos($data, 'jpeg') !== false) || (strpos($data, 'jpg') !== false)
                    || (strpos($data, 'png') !== false)) {
                $new_file_in_cache_path = str_replace("/", "_", $public_path);

                $files_in_cache = getDataFromDirectory($cacheDirectory);

                $files_already_exists_in_cache = false;
                foreach ($files_in_cache as $cache_file) {
                  if ($cache_file == $new_file_in_cache_path) {
                    $files_already_exists_in_cache = true;
                  }
                }

                if (!$files_already_exists_in_cache) {
                  $resizedFile = $cacheDirectory . $new_file_in_cache_path;

                  smart_resize_image($exact_path, null, 45, 45, false, $resizedFile, false, false, 100);
                }

                $link = '<a class="colorable_link venobox" target="_blank" data-gall="pictures_link" href="' . $exact_path . '">' . $data . '</a>';
                $preview = '<a class="venobox" href="' . $exact_path . '" data-gall="pictures_preview"><img src="' . $cacheDirectory . $new_file_in_cache_path . '"></a>';
              }
            }

            $size = getAwesomeSize($size);

            echo '<tr class="hoverable">';

            echo '<td>' . $link . '</td>';
            echo '<td width="10%">' . $preview . '</td>';
            echo '<td width="15%">' . getLastEditedDate($exact_path) . '</td>';
            echo '<td width="10%">' . $size . '</td>';
            echo '
                <td width="20%">
                  <div hidden id="copy_link_' . $exact_path . '">' . $sharePath . '</div>
                  <a class="dropdown-trigger colorable_link" href="#" data-target="dropdown_' . $exact_path . '"><i class="material-icons">more_vert</i></a>
                   <!-- Dropdown Structure -->
                  <ul id="dropdown_' . $exact_path . '" class="dropdown-content colorable_dropdown">
                    <li>
                      <a class="colorable_link black-text" 
                          onclick="copyToClipboard(document.getElementById(\'copy_link_' . $exact_path . '\').innerHTML)">
                        <i class="material-icons">share</i>Copy link
                      </a>
                    </li>
                    <li>
                      <a href="mailto:enterAn@Emailaddress.Here?subject=Hey, check this files out!
                          &body=' . $sharePath . '" class="colorable_link"> <i class="material-icons">mail</i>
                        Share via mail
                      </a>
                    </li>
                  </ul>
                 
                </td>';

            if (getLoggedIn()) {
              echo '<td width="6%">
                    <a class="btn-floating red modal-trigger tooltipped waves-effect" data-position="left"
                        data-tooltip="Delete this item" 
                        href="./index.php?file=' . $exact_path . '&action=delete';
              if ($isInSubdirectory) echo '&directory=' . $subdirectorypath;
              echo '">
                      <i class="material-icons">delete</i>
                    </a>
                  </td>';

            }
            echo '</tr>';
          }
        }
        ?>
        </tbody>
      </table>

      <?php
      if ($folderIsEmpty) {
        echo '<h5 class="colorable_text black-text">Ohh no, it looks really empty here :(</h5>';
      }

      ?>
    </div>
  </div>

  <?php
  if (getLoggedIn()) {
    echo '
    <div class="fixed-action-btn">
      <btn class="btn-floating btn-large green waves-effect waves-light" id="adminMenu">
        <i class="large material-icons">mode_edit</i>
      </btn>
      <ul>
        <li>
          <a class="btn-floating waves-effect waves-light blue tooltipped modal-trigger" data-position="top" 
              data-tooltip="Create a directory" href="#modal_create_directory">
            <i class="material-icons">create</i>
          </a>
        </li>
        <li>
          <a class="btn-floating waves-effect waves-light purple tooltipped modal-trigger" data-position="top" 
              data-tooltip="File upload" href="#modal_upload_file">
            <i class="material-icons">file_upload</i>
          </a>
        </li>
      </ul>
    </div>
  ';
  }
  ?>

  <?php
  if (getLoggedIn()) {
    echo getCreateDirectoryModal($isInSubdirectory, $subdirectorypath);

    echo getUploadFileModal($isInSubdirectory, $subdirectorypath);

    echo '
      <!-- Discovery feature -->
      <div id="adminMenuTapTarget" class="tap-target green" data-target="adminMenu">
        <div class="tap-target-content">
          <h5>Welcome</h5>
          <p>in the admin menu. Hover the button to create folders and upload files.</p>
        </div>
      </div>
      ';

  } else {
    echo getLoginModal($isInSubdirectory, $subdirectorypath);
  }

  echo getAboutModal();

  ?>

  <!--  Scripts-->
  <script type="text/javascript" src="./js/checkForInternetExplorer.js"></script>
  <script type="text/javascript" src="./js/materialize.js"></script>
  <script type="text/javascript" src="./js/jquery.js"></script>
  <script type="text/javascript" src="./js/venobox.min.js"></script>
  <script type="text/javascript" src="js/serverHandler.js"></script>
  <script type="text/javascript" src="./js/theming.js"></script>
  <script type="text/javascript">
    <?php
    if ($alert != null) {
      switch ($alert) {
        case 'logoutSuccessful':
          echo showAlert('<span>Logout successful</span><a class="btn-flat toast-action modal-trigger" href="#modal_login">Log in again</a>');

          break;
        case 'wrongCredentials':
          echo showAlert('<span>Login credentials wrong</span><a class="btn-flat toast-action modal-trigger" href="#modal_login">Try again</a>');

          break;
        case 'loginSuccessful':
          echo showAlert('<span>Login successful</span><a class="btn-flat toast-action" href="index.php?action=logout">Logout</a>');

          break;
        case 'directory_exists':
          echo showAlert('<span>Could not create directory because it already exists!</span>');

          break;
        case 'successful_created_directory':
          echo showAlert('<span>Directory successful created!</span>');

          break;
        case 'permission_denied':
          echo showAlert('<span>You do have not enough permissions to perform this action!</span>');

          break;
        case 'fileExists':
          echo showAlert('<span>Could not upload this file because it already exists!</span>');

          break;
        case 'fileToBig':
          echo showAlert('<span>Could not upload this file because it is to big!</span>');

          break;
        case 'file_successful_uploaded':
          echo showAlert('<span>File was successfully uploaded!</span>');

          break;
        case 'file_uploaded_error':
          echo showAlert('<span>Could not upload this file!</span>');

          break;
        case 'successful_deleted_dir':
          echo showAlert('<span>Sucessfull deleted the directory!</span>');

          break;
        case 'successful_deleted_file':
          echo showAlert('<span>Sucessfull deleted the file!</span>');

          break;
        case 'file_not_exists':
          echo showAlert('<span>Could not delete anything because it does not exist!</span>');

          break;
        case 'successful_cache_cleared':
          echo showAlert('<span>Cache was successfully cleared!</span>');

          break;
      }
    }
    ?>

    $(document).ready(function () {
      $('.venobox').venobox({
        arrowsColor: '#FFFFFF',
        numeratio: true,
        infinigall: true,
        closeBackground: '#000000',
        closeColor: '#FFFFFF',
        spinner: 'wandering-cubes'
      });
    });

    function copyToClipboard(text) {
      window.prompt("Copy to clipboard: Ctrl+C, Enter", text);
    }

    <?php
    if (getLoggedIn()) {
      echo '
      if(!getCookie("showedTapTarget")) {
        setCookie("showedTapTarget", true, "Thu, 18 Dec 2020 12:00:00 UTC")
        showAdminMenuTapTarget();
      }
      ';
    }
    ?>
  </script>

</body>
</html>
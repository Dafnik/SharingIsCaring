<?php
require_once(__DIR__ . '/../version.php');

function getLoginModal($isInSubdirectory, $subdirectorypath)
{
  $content = '
  <div id="modal_login" class="modal">
    <form method="POST" action="./index.php">
      <div class="modal-content">
        <h4>Login</h4>

        <div class="row">
          <div class="input-field col s6">
            <i class="material-icons prefix">account_circle</i>
            <input id="username" name="username" type="text" required autofocus>
            <label for="username">Username</label>
          </div>
          <div class="input-field col s6">
            <i class="material-icons prefix">lock</i>
            <input id="password" name="password" type="password" required>
            <label for="password">Password</label>
          </div>
        </div>
        <input type="hidden" name="action" value="login">
  ';

  if ($isInSubdirectory) {
    $content = $content . '<input type="hidden" name="directory" value="' . $subdirectorypath . '">';
  }

  $content = $content . '
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn-flat green waves-effect waves-light modal-close">Log in</button>
        <a class="btn-flat red modal-action modal-close waves-effect waves-light">Close</a>
      </div>
    </form>
  </div>
  ';

  return $content;
}

function getAboutModal()
{
  return '
  <div id="modal_about" class="modal modal-fixed-footer">
    <div class="modal-content">
      <h4>Informations</h4>

      <ul class="collection with-header hoverable">
        <li class="collection-header"><h5>Contributor</h5></li>
        <li class="collection-item">Developer: Dominik Dafert</li>
         <li class="collection-item">Idea: Bernhard Steindl & Dominik Dafert</li>
      </ul>

      <ul class="collection with-header hoverable">
        <li class="collection-header"><h5>Libraries</h5></li>
        <li class="collection-item">Materialize v1.0.0-rc.1</li>
        <li class="collection-item">jQuery v3.3.1</li>
        <li class="collection-item">Venobox - jQuery Plugin v1.8.3</li>
      </ul>

      <ul class="collection with-header hoverable">
        <li class="collection-header"><h5>Licence: APACHE-2.0</h5></li>
        <li class="collection-item">Version: ' . getVersion() . '</li>
        <li class="collection-item">Source Code: <a href="https://github.com/Dafnik/SharingIsCaring/">GitHub</a></li>
      </ul>

    </div>
    <div class="modal-footer">
      <button class="btn-flat red modal-action modal-close waves-effect waves-light">Close</button>
    </div>
  </div>
  ';
}

function getCreateDirectoryModal($isInSubdirectory, $subdirectorypath)
{
  $content = '
  <div id="modal_create_directory" class="modal">
    <form method="POST" action="./index.php">
      <div class="modal-content">
        <h4>Create a directory</h4>

        <div class="row">
          <div class="input-field col s6">
            <i class="material-icons prefix">create</i>
            <input id="directory_name" name="directory_name" type="text" required autofocus>
            <label for="directory_name">Enter the directory name</label>
          </div>
        </div>
        <input type="hidden" name="action" value="create_directory">
        ';
  if ($isInSubdirectory) {
    $content = $content . '<input type="hidden" name="directory" value="' . $subdirectorypath . '">';
  }

  $content = $content . '
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn-flat green waves-effect waves-light modal-close">Create</button>
        <a class="btn-flat red modal-action modal-close waves-effect waves-light">Close</a>
      </div>
    </form>
  </div>
  ';

  return $content;
}

function getUploadFileModal($isInSubdirectory, $subdirectorypath)
{
  $content = '
  <div id="modal_upload_file" class="modal">
    <form method="POST" action="./index.php" enctype="multipart/form-data">
      <div class="modal-content">
        <h4>Upload one file or multiple files</h4>

        <div class="row">
          <div class="file-field input-field">
            <div class="btn">
              <span>Files</span>
              <input type="file" name="fileToUpload[]" id="fileToUpload" multiple="multiple">
            </div>
            <div class="file-path-wrapper">
              <input class="file-path validate" type="text">
            </div>
          </div>
        </div>
        <input type="hidden" name="action" value="upload_a_file">
        ';

  if ($isInSubdirectory) {
    $content = $content . '<input type="hidden" name="directory" value="' . $subdirectorypath . '">';
  }

  $content = $content . '

      </div>
      <div class="modal-footer">
        <button type="submit" class="btn-flat green waves-effect waves-light modal-close">Upload</button>
        <a class="btn-flat red modal-action modal-close waves-effect waves-light">Close</a>
      </div>
    </form>
  </div>
  ';

  return $content;
}
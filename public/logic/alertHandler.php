<?php

function showAlert($content) {
  return '
    var toastHTML = \''.$content.'\';
    M.toast({html: toastHTML});
  ';
}
<?php

function getLoggedIn() {
  if (!isset($_SESSION)) {
    session_start();
  }

  if(!isset($_SESSION['isLoggedIn'])) {
    $_SESSION['isLoggedIn'] = false;
  }

  return $_SESSION['isLoggedIn'];
}

function login($username, $password) {
  if (!isset($_SESSION)) {
    session_start();
  }

  if (($username == getSettings()['username']) && $password == getSettings()['password']) {
    $_SESSION['isLoggedIn'] = true;

    return 'loginSuccessful';
  } else {
    $_SESSION['isLoggedIn'] = false;

    return "wrongCredentials";
  }
}

function logout() {
  if(!isset($_SESSION)) {
    session_start();
  }

  $_SESSION['isLoggedIn'] = false;
}
<?php

require_once(__DIR__ .'/authHandler.php');

define("CONST_SETTINGS_FILE", __DIR__."/../../private/settings.json");

$res = "";
if (isset($_REQUEST['action'])) {
  switch ($_REQUEST['action']) {
    case 'save':
      $res = saveSettings($_REQUEST['json']);
      break;
  }
  echo $res;
}

function saveSettings($json) {
  try {
    $file = getSettingsFile();
    fwrite($file, $json);
    fflush($file);
    fclose($file);
  } catch (Exception $e) {
    return $e->getMessage();
  }
  return 'true';
}

function getSettingsFile() {
  return fopen(CONST_SETTINGS_FILE, "w+");
}

function getSettings() {
  return json_decode(file_get_contents(CONST_SETTINGS_FILE), true);
}
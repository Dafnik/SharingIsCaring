<?php

require_once (__DIR__ . '/authHandler.php');
require_once (__DIR__ . '/settingsHandler.php');

function getDataFromDirectory($path) {
  return array_diff(scandir($path), array('..', '.'));
}

function isDirectory($path) {
  return is_dir($path);
}

function getDirectorySize($path) {
  $bytestotal = 0;
  $path = realpath($path);
  if($path!==false && $path!='' && file_exists($path)){
    foreach(new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path, FilesystemIterator::SKIP_DOTS)) as $object){
      $bytestotal += $object->getSize();
    }
  }

  return $bytestotal;
}

function getFileSize($path) {
  return filesize($path);
}

function getLastEditedDate($path) {
  return date ("d F Y H:i:s", filemtime($path));
}

function getAwesomeSize($size) {
  if($size > 1000000000000) {
    $size = $size / 1000000000000;
    $size = round($size, 4);
    $size = $size . ' TB';
  } else if($size > 1000000000) {
    $size = $size / 1000000000;
    $size = round($size, 3);
    $size = $size . ' GB';
  } else if($size > 1000000) {
    $size = $size / 1000000;
    $size = round($size, 2);
    $size = $size . ' MB';
  } else if($size > 1000) {
    $size = $size / 1000;
    $size = round($size, 2);
    $size = $size . ' KB';
  } else {
    $size = $size . ' B';
  }

  return $size;
}

function createDirectory($subdirectory = null, $directoryName) {
  if(getLoggedIn()) {
    $rootDirectory = './files/';

    $directory = $rootDirectory . $subdirectory . '/' . $directoryName;

    if (file_exists($directory) AND is_dir($directory)) {
      return 'directory_exists';
    }

    mkdir($directory);
    return 'successful_created_directory';
  }
  return 'permission_denied';
}

function uploadAFile($subdirectory, $filename, $size, $tmp_name) {
  if(getLoggedIn()) {
    $rootDirectory = './files/';

    $file = $rootDirectory . $subdirectory . '/' . $filename;

    if (file_exists($file)) {
      return 'fileExists';
    }

    if ($size > getSettings()['maxFileSize']) {
      return 'fileToBig';
    }

    if (move_uploaded_file($tmp_name, $file)) {
      return 'file_successful_uploaded';
    } else {
      return 'file_uploaded_error';
    }
  }

  return 'permission_denied';
}

function delete($path) {
  if(getLoggedIn()) {
    if(file_exists($path)) {
      if(is_dir($path)) {
        rmdir($path);

        return 'successful_deleted_dir';
      } else {
        unlink($path);

        return 'successful_deleted_file';
      }
    }

    return 'file_not_exists';
  }

  return 'permission_denied';
}

function clearCache() {
  if(getLoggedIn()) {

    $cachePath = __DIR__ .'/../cache/';

    $directory_data = getDataFromDirectory($cachePath);

    foreach ($directory_data as $data) {
      $exact_path = $cachePath . $data;

      if($exact_path != __DIR__ .'/../cache/README.md') {
        unlink($exact_path);
      }
    }

    return 'successful_cache_cleared';
  }

  return 'permission_denied';
}
<?php
$settingsFile = __DIR__ . '/../private/settings.json';

ini_set('display_errors', 'On');
error_reporting(E_ALL | E_STRICT);

if(file_exists($settingsFile)) {
  header("Location: ./index.php");
  die();
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>SIC - Setup</title>

  <link rel="shortcut icon" type="image/favicon.ico" href="images/favicon.ico">

  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>

  <!-- CSS  -->
  <link href="fonts/materialicon.css" rel="stylesheet">
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>

<div class="container">
  <div class="row">
    <div class="col s12 m12 l12 xl12">
      <div class="card white">
        <div class="card-content">
          <div class="row">
            <h2 class="center-align">Welcome to Sharing Is Caring</h2>
          </div>
          <div class="row">
            <form class="col s12">
              <div class="row">
                <div class="input-field col s6">
                  <i class="material-icons prefix">account_circle</i>
                  <input id="username" name="username" type="text" autofocus>
                  <label for="username">Username</label>
                </div>
                <div class="input-field col s6">
                  <i class="material-icons prefix">lock</i>
                  <input id="password" name="password" type="password">
                  <label for="password">Password</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s6">
                  <i class="material-icons prefix">cloud_upload</i>
                  <input id="maxFileSize" name="username" type="number">
                  <label for="maxFileSize">Max file upload size</label>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="card-action">
          <a href="#" onclick="saveSettings()">Finish Setup</a>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="./js/checkForInternetExplorer.js"></script>
<script type="text/javascript" src="./js/materialize.js"></script>
<script type="text/javascript" src="./js/jquery.js"></script>
<script type="text/javascript" src="js/serverHandler.js"></script>
<script type="text/javascript">
  function saveSettings() {

    var settings = {
      "username": $('#username').val(),
      "password": $('#password').val(),
      "maxFileSize": $('#maxFileSize').val()
    };

    var cfunc = function (xhttp) {
      if (xhttp.responseText == 'true') {
        window.location = "index.php";
      } else
        console.error(xhttp.responseText);
    };

    xhr_post("logic/settingsHandler.php", cfunc, "json=" + JSON.stringify(settings, null, "\t") + "&action=save");

  }
</script>

</body>
</html>



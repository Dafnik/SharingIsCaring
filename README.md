# SharingIsCaring
A little materialized HTTP web server indexer


## 1.0) Features
1. Folder support
1. Images
    1. Online image viewer
    1. Image will be calculated smaller and there is a preview available
1. Online administration system
    1. Online directory creating
    1. Online file uploader
    1. Online file and directory deleting
1. Compatible with every web server
1. Share feature
1. Working without javascript


## 2.0) Screenshots

### 2.1) Dark UI:
#### 2.1.1) Folder view
![Complete_DARK_FOLDER_UI](screenshots/dark_ui.png)

### 2.2) Light UI:
#### 2.2.1) Folder view
![Complete_LIGHT_FOLDER_UI](screenshots/light_ui.png)


## 3.0) Web server
Generally speaking, Sharing Is Caring should be compatible with any web server. You should be able to
set it up without any special configuration files.

### 3.1) Web server setup
Just make the ```public``` directory the root web server directory and link the ```index.php``` in
the website config.

### 3.2) Permissions and access control
I do not guarantee any security. Control access is something web server special and I can not control
it.


## 4.0) Setup
At the first run of Sharing Is Caring you will be asked to enter user credentials and the maximal 
file upload size. **Don't forget, the maximal file upload size is only application only, maybe you
have to change the PHP maximal upload size in the ```php.ini``` file also!** 


## 5.0) File system
All files placed in the ```files``` directory are listed in the root directory. You can also create
sub folders.

### 5.1) Permissions
The ```www-data``` user must have full access to the ```SharingIsCaring``` folder.


## 6.0) Admin system
Sharing Is Caring has an administration system.

### 6.1) Log in
The user credentials mentioned earlier and declared at the first run of Sharing Is Caring had to be
entered after clicking at the ```Login``` button.

### 6.2) File uploading
You are allowed to upload any file directly to Sharing Is Caring. Note that the uploading process is
currently not handled asynchron. This means that if you upload huge files it may be occurs that you
see a blank site for some minutes.

### 6.3) Creating a folder
It's also possible to create a folder directly over the web ui.

### 6.4) Deleting
You can delete files and folders directly over the web ui.